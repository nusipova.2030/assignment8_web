import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  navItems = [
    {
      title: 'Users',
      link: 'users'
    },
    {
      title: 'Groups',
      link: 'groups'
    }

  ]
  constructor() { }

  ngOnInit(): void {
  }

}
